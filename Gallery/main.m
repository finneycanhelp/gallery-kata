//
//  main.m
//  Gallery
//
//  Created by Michael Finney on 9/9/13.
//  Copyright (c) 2013 Michael Finney. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GGAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GGAppDelegate class]));
    }
}
