//
//  GGAppDelegate.h
//  Gallery
//
//  Created by Michael Finney on 9/9/13.
//  Copyright (c) 2013 Michael Finney. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GGViewController;

@interface GGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) GGViewController *viewController;

@end
